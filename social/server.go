package main

import (
	"social/controller"
	"social/services"

	"github.com/gin-gonic/gin"
)

var (
	ideaService    services.IdeaService      = services.New()
	ideaController controller.IdeaController = controller.New(ideaService)
)

func main() {
	server := gin.Default()
	server.GET("/new", func(ctx *gin.Context) {
		ctx.JSON(200, ideaController.GetAll())
	})
	server.POST("/new", func(ctx *gin.Context) {
		ctx.JSON(200, ideaController.Save(ctx))
	})
	server.Run(":8080")
}
