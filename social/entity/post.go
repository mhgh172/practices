package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Idea struct {
	ID    primitive.ObjectID `json:"ID" bson:"_id"`
	Name  string             `json:"name" bson:"Name"`
	Date  time.Time          `json:"date" bson:"Date"`
	Title string             `json:"title" bson:"Title"`
	Body  string             `json:"body" bson:"Body"`
}
