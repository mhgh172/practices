package controller

import (
	"social/entity"
	"social/services"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
)

type IdeaController interface {
	Save(ctx *gin.Context) entity.Idea
	GetAll() []bson.M
}

type controller struct {
	srv services.IdeaService
}

func New(service services.IdeaService) IdeaController {
	return &controller{
		srv: service,
	}
}

func (c *controller) GetAll() []bson.M {
	return c.srv.GetAll()
}

func (c *controller) Save(ctx *gin.Context) entity.Idea {
	var idea entity.Idea
	ctx.BindJSON(&idea)
	return c.srv.Save(idea)
}
