package services

import (
	"context"
	"log"
	"social/db"
	"social/entity"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IdeaService interface {
	Save(entity.Idea) entity.Idea
	GetAll() []bson.M
}

type ideaService struct {
	ideas []entity.Idea
}

func New() IdeaService {
	return &ideaService{}
}

func (idea *ideaService) Save(ed entity.Idea) entity.Idea {
	dtbs := db.DbConnect()
	ed.Date = time.Now()
	ideacol := dtbs.Collection("idea")
	result, err := ideacol.InsertOne(context.Background(), bson.M{
		"Name":  ed.Name,
		"Title": ed.Title,
		"Date":  ed.Date,
		"Body":  ed.Body,
	})
	if err != nil {
		log.Panic(err)
	}
	ed.ID = result.InsertedID.(primitive.ObjectID)
	return ed
}

func (idea *ideaService) GetAll() []bson.M {
	dtbs := db.DbConnect()
	ideacol := dtbs.Collection("idea")
	var result []bson.M
	cursor, err := ideacol.Find(context.Background(), bson.D{})
	if err != nil {
		log.Panic(err)
	}
	if err = cursor.All(context.Background(), &result); err != nil {
		log.Panic(err)
	}
	return result
}
