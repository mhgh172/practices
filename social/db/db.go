package db

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var Database *mongo.Database

func DbConnect() *mongo.Database {
	if Database == nil {
		Client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
		if err != nil {
			log.Panic(err)
		}
		Ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
		err = Client.Connect(Ctx)
		if err != nil {
			log.Panic(err)
		}
		Database = Client.Database("Ideas")
		return Database
	}
	return Database
}
